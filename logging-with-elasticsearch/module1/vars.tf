variable "ACCESS_KEY" {}
variable "SECRET_KEY" {}

variable "INSTANCE_TYPE" {
  default = "t2.micro"
}

variable "REGION" {
  default = "eu-west-1"
}

variable "KEY_NAME" {
  default = "TU_Dublin"
}

variable "EC2TAGS" {
  default = "Module 1 EC2 Instance"
}

variable "INPUT_CIDR" {
  default = "0.0.0.0/0"
}

variable "INPUT_CIDR_AWS" {
  default = "172.0.0.0/8"
}

variable "OUTPUT_CIDR" {
  default = "0.0.0.0/0"
}

variable "INPUT_SSH_PORT" {
  default = "22"
}

variable "INPUT_HTTP_PORT" {
  default     = "8000"
}

variable "INPUT_PROTOCOL" {
  default = "tcp"
}

variable "OUTPUT_PORT_START" {
  default = "0"
}

variable "OUTPUT_PORT_END" {
  default = "0"
}
