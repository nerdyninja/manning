resource "aws_launch_configuration" "module1LC" {
  name = "module1_Launch_Config"
  image_id = "ami-04d5cc9b88f9d1d39"
  instance_type = var.INSTANCE_TYPE
  key_name = var.KEY_NAME
  security_groups = [aws_security_group.module1SGInternal.id]

  lifecycle {
    create_before_destroy = true
  }

  user_data = <<-EOF
                #!/bin/bash
                sudo apt-get remove docker docker-engine docker.io containerd runc
                curl -fsSL https://get.docker.com -o get-docker.sh
                sudo sh get-docker.sh
                docker pull wordpress:php7.4-fpm-alpine
                docker pull chentex/random-logger:latest

                docker run --name wordpress:php7.4-fpm-alpine -p 8000:80 -d wordpress
                docker run chentex/random-logger:latest 100 400
                EOF
}

resource "aws_autoscaling_group" "module1ASG" {
  name = "module1_AutoScaler_Group"
  availability_zones   = data.aws_availability_zones.all.names
  launch_configuration = aws_launch_configuration.module1LC.id

  load_balancers    = [aws_elb.module1LB.name]
  health_check_type = "ELB"

  min_size = 2
  max_size = 2
}

resource "aws_elb" "module1LB" {
  name = "module1LoadBalancer"
  availability_zones = data.aws_availability_zones.all.names
  security_groups = [aws_security_group.module1SG.id]

  # This adds a heartbeat for servers.
  health_check {
    target              = "HTTP:${var.INPUT_HTTP_PORT}/"
    interval            = 60
    timeout             = 50
    healthy_threshold   = 2
    unhealthy_threshold = 10
  }

  # This adds a listener for incoming HTTP requests.
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = var.INPUT_HTTP_PORT
    instance_protocol = "http"
  }
}


resource "aws_security_group" "module1SG" {
  name = "module1_Security_Group"
  # SSH access from anywhere
  ingress {
    from_port   = var.INPUT_SSH_PORT
    to_port     = var.INPUT_SSH_PORT
    protocol    = var.INPUT_PROTOCOL
    cidr_blocks = [var.INPUT_CIDR]
  }

  # HTTP access from anywhere
  ingress {
    from_port   = var.INPUT_HTTP_PORT
    to_port     = var.INPUT_HTTP_PORT
    protocol    = var.INPUT_PROTOCOL
    cidr_blocks = [var.INPUT_CIDR]
  }

  # Inbound HTTP from anywhere, to LB
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = var.INPUT_PROTOCOL
    cidr_blocks = [var.INPUT_CIDR]
  }

  # outbound internet access
  egress {
    from_port   = var.OUTPUT_PORT_START
    to_port     = var.OUTPUT_PORT_END
    # -1 in the protocol means all protocols
    protocol    = "-1"
    cidr_blocks = [var.OUTPUT_CIDR]
  }
}

resource "aws_security_group" "module1SGInternal" {
  name = "module1_AWS_Security_Group"

  # SSH access from anywhere
  ingress {
    from_port   = var.INPUT_SSH_PORT
    to_port     = var.INPUT_SSH_PORT
    protocol    = var.INPUT_PROTOCOL
    cidr_blocks = [var.INPUT_CIDR]
  }

  # HTTP access from AWS
  ingress {
    from_port   = var.INPUT_HTTP_PORT
    to_port     = var.INPUT_HTTP_PORT
    protocol    = var.INPUT_PROTOCOL
    cidr_blocks = [var.INPUT_CIDR_AWS]
  }

  # outbound internet access
  egress {
    from_port   = var.OUTPUT_PORT_START
    to_port     = var.OUTPUT_PORT_END
    # -1 in the protocol means all protocols
    protocol    = "-1"
    cidr_blocks = [var.OUTPUT_CIDR]
  }
}
